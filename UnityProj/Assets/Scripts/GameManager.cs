﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LevelManager))]
public class GameManager : MonoBehaviour
{
    public CameraCraneScript cameraCrane;
    public Text lblPoints;

    public AudioSource audioSiren;
    public AudioSource audioChomp;
    public AudioSource audioJingle;
    public AudioSource audioDeath;

    private LevelManager levelMgr;
    private PlayerController player;
    private List<PillScript> pills;
    private List<GhostController> ghosts;
    private int numOfPills;
    private int points;

    private bool timerActive;
    private float timerTimeout;
    private float timerStart;
    private Action timerCallback;

    public void Start()
    {
        if (lblPoints == null)
            throw new Exception("lblPoints == null");

        if (audioSiren == null)
            throw new Exception("audioSiren == null");

        if (audioChomp == null)
            throw new Exception("audioChomp == null");

        if (audioDeath == null)
            throw new Exception("audioDeath == null");

        points = 0;
        timerActive = false;
        InitLevel();
        InitCamera();
    }

    public void Update()
    {
        if (timerActive)
        {
            if (Time.time - timerStart >= timerTimeout)
            {
                timerActive = false;

                if (timerCallback != null)
                    timerCallback();
            }
        }
    }

    private void UpdatePointsLabel()
    {
        lblPoints.text = "Points: " + points.ToString("0000000");
    }

    private void SetTimer(float timeout, Action callback)
    {
        timerActive = true;
        timerStart = Time.time;
        timerTimeout = timeout;
        timerCallback = callback;
    }

    private void InitLevel()
    {
        points = 0;

        // Build level.
        levelMgr = GetComponent<LevelManager>();
        levelMgr.InitLevel();

        // Init player.
        player = levelMgr.Player;

        // Init ghosts.
        InitGhosts();

        // Init pills.
        InitPills();
    }

    private void InitPills()
    {
        pills = levelMgr.Pills;

        foreach (var pill in pills)
        {
            pill.gameObject.SetActive(true);
            pill.OnEaten = OnPillEat;
        }

        numOfPills = pills.Count;

        audioSiren.pitch = GameSettings.sirenMinPitch;

        player.Pause();

        foreach (var g in ghosts)
            g.Pause();

        audioJingle.Play();
        SetTimer(audioJingle.clip.length, () =>
        {
            player.Unpause();

            foreach (var ghost in ghosts)
                ghost.Unpause();

            audioSiren.Play();
        });
    }

    private void OnPillEat(PillScript pill)
    {
        numOfPills--;
        pill.gameObject.SetActive(false);

        audioSiren.pitch = 
            Mathf.Lerp(
                GameSettings.sirenMaxPitch,
                GameSettings.sirenMinPitch,
                (float)numOfPills / (float)pills.Count
            );

        points += 10;
        UpdatePointsLabel();

        if (numOfPills == 0)
        {
            audioChomp.Stop();
            audioSiren.Stop();

            PauseAndResetCharacters(callback: () =>
            {
                points = 0;
                UpdatePointsLabel();
                InitPills();
            });
        }

        if (!audioChomp.isPlaying)
            audioChomp.Play();
    }

    private void InitGhosts()
    {
        ghosts = levelMgr.Ghosts;

        foreach (var ghost in ghosts)
            ghost.OnTouchPlayer = OnGhostTouchPlayer;
    }

    private void OnGhostTouchPlayer()
    {
        audioChomp.Stop();
        audioSiren.Stop();
        audioDeath.Play();

        PauseAndResetCharacters(audioDeath.clip.length, () =>
        {
            audioSiren.Play();
        });
    }

    private void PauseAndResetCharacters(float duration = 1f, Action callback = null)
    {
        foreach (var ghost in ghosts)
            ghost.Pause();

        player.Pause();

        SetTimer(duration, () =>
        {
            player.ResetPosition();
            player.Unpause();

            foreach (var ghost in ghosts)
            {
                ghost.ResetPosition();
                ghost.Unpause();
            }

            if (callback != null)
                callback();
        });   
    }

    private void InitCamera()
    {
        if (cameraCrane == null)
            throw new Exception("Camera crane is null.");

        if (player == null)
            throw new Exception("player is null.");

        if (player.gameObject == null)
            throw new Exception("player.gameObject is null.");

        cameraCrane.target = player.gameObject.transform;
    }
}
