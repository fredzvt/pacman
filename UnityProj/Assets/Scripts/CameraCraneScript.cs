﻿using System;
using UnityEngine;

public class CameraCraneScript : MonoBehaviour
{
    public Transform target;
    public float velocity = 1f;

    private float angleRange = 10f;
    private float halfAngleRange;
    private float initXAngle;
    private float initZAngle;
    private float initYAngle;

    public void Start()
    {
        halfAngleRange = angleRange / 2;

        var initRotation = transform.rotation.eulerAngles;
        initXAngle = initRotation.x;
        initYAngle = initRotation.y;
        initZAngle = initRotation.z;
    }

    public void Update()
    {
        var maxColPos = (GameSettings.colLength - 1) * GameSettings.gridSize;
        var currColPercentual = target.position.x * 100 / maxColPos;
        var currColAngleToAdd = angleRange * currColPercentual / 100;
        currColAngleToAdd -= halfAngleRange;
        currColAngleToAdd *= -1;
        var currColAngle = initZAngle + currColAngleToAdd;

        var maxRowPos = (GameSettings.rowLength - 1) * GameSettings.gridSize;
        var currRowPercentual = target.position.z * 100 / maxRowPos;
        var currRowAngleToAdd = angleRange * currRowPercentual / 100;
        currRowAngleToAdd -= halfAngleRange;
        var currRowAngle = initXAngle + currRowAngleToAdd;

        var newRotation = Quaternion.Euler(currRowAngle, initYAngle, currColAngle);
        var lerpedRotation = Quaternion.Lerp(transform.localRotation, newRotation, Time.deltaTime * velocity);

        transform.localRotation = lerpedRotation;
    }
}
