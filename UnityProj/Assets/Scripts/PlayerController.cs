﻿using UnityEngine;
using System;

public class PlayerController : MonoBehaviour
{
    public bool showDebugInfo = false;

    private bool pause;
    private Direction currDirection;
    private IntVector3 intPosition;
    private int speed = 2;
    private Direction? requestedDirection;
    private float lastInputTime;
    private float periodToRememberLastInput = .5f;
    private bool isGridAligned;
    private int initialRow;
    private int initialCol;

    public void OnGUI()
    {
        if (showDebugInfo)
        {
            GUILayout.TextArea("Grid Aligned: " + isGridAligned);
            GUILayout.TextArea("GO Pos: " + transform.position);
            GUILayout.TextArea("Int Pos: " + intPosition);
            GUILayout.TextArea("Curr Direction: " + currDirection);
            GUILayout.TextArea("Requested Direction: " + requestedDirection);
        }
    }

    public void Start()
    {
        currDirection = GameSettings.playerInitialDirection;

        UpdateRotation();

        if (intPosition == null)
            intPosition = IntVector3.Zero;
    }

    public void Update()
    {
        if (!pause)
        {
            HandleInput();
            IntVector3 nextIntPosition = null;

            if (
                (requestedDirection.HasValue && requestedDirection.Value != currDirection) &&
                (
                    RequestedDirectionIsOnTheSameAxis() ||
                    (isGridAligned && !IsFacingWall(requestedDirection.Value))
                )
               )
            {
                nextIntPosition = GetNextPosition(requestedDirection.Value);
                currDirection = requestedDirection.Value;
            }
            else
            {
                if (isGridAligned && IsFacingWall(currDirection))
                {
                    nextIntPosition = intPosition;
                }
                else
                {
                    nextIntPosition = GetNextPosition(currDirection);
                }
            }

            UpdatePosition(nextIntPosition);
            UpdateRotation();
        }
    }

    public void UpdateRotation()
    {
        switch (currDirection)
        {
            case Direction.Up:
                transform.rotation = Quaternion.Euler(0, 180f, 0);
                break;

            case Direction.Down:
                transform.rotation = Quaternion.identity;
                break;

            case Direction.Left:
                transform.rotation = Quaternion.Euler(0, 90f, 0);
                break;

            case Direction.Right:
                transform.rotation = Quaternion.Euler(0, 270f, 0);
                break;

            default:
                throw new Exception("Unexpected direction.");
        }
    }
    
    public void SetResetPosition(int row, int col)
    {
        initialRow = row;
        initialCol = col;

        ResetPosition();
    }

    public void ResetPosition()
    {
        currDirection = GameSettings.playerInitialDirection;

        var pos =
            new IntVector3(
                initialCol * GameSettings.gridSize,
                0,
                -(initialRow * GameSettings.gridSize)
            );

        UpdatePosition(pos);
    }

    public void Pause()
    {
        pause = true;
    }

    public void Unpause()
    {
        pause = false;
    }

    private bool IsFacingWall(Direction direction)
    {
        var gridPos = intPosition.ToGridPosition();

        switch (direction)
        {
            case Direction.Up:
                gridPos.Row--;
                break;

            case Direction.Down:
                gridPos.Row++;
                break;

            case Direction.Left:
                gridPos.Col--;
                break;

            case Direction.Right:
                gridPos.Col++;
                break;

            default:
                throw new Exception("Unexpected direction.");
        }

        if (gridPos.Row < 0 || 
            gridPos.Col < 0 || 
            gridPos.Row > GameSettings.rowLength - 1 ||
            gridPos.Col > GameSettings.colLength - 1)
            return true;

        var facingWall =
            GameSettings.level[gridPos.Row, gridPos.Col] == (int)LevelTileType.Wall ||
            GameSettings.level[gridPos.Row, gridPos.Col] == (int)LevelTileType.InvisibleWall;

        return facingWall;
    }

    private void UpdatePosition(IntVector3 vec)
    {
        if (vec == null)
            throw new ArgumentNullException("vec");

        intPosition = vec;
        transform.position = intPosition.ToVector3();

        isGridAligned =
            intPosition.X % GameSettings.gridSize == 0 &&
            intPosition.Z % GameSettings.gridSize == 0;
    }

    private IntVector3 GetNextPosition(Direction direction)
    {
        var nextIntPosition = intPosition.Copy();

        switch (direction)
        {
            case Direction.Up:
                nextIntPosition.Z += speed;
                break;

            case Direction.Down:
                nextIntPosition.Z -= speed;
                break;

            case Direction.Left:
                nextIntPosition.X -= speed;
                break;

            case Direction.Right:
                nextIntPosition.X += speed;
                break;

            default:
                throw new Exception("Unexpected direction.");
        }

        return nextIntPosition;
    }

    private void HandleInput()
    {
        var input = GetPlayerInput();

        if (input.HasValue)
        {
            requestedDirection = input.Value;
            lastInputTime = Time.time;
        }
        else
        {
            if (Time.time - lastInputTime > periodToRememberLastInput)
            {
                requestedDirection = null;
            }
        }
    }

    private Direction? GetPlayerInput()
    {
        if (Input.GetKey(KeyCode.UpArrow))
            return Direction.Up;

        if (Input.GetKey(KeyCode.DownArrow))
            return Direction.Down;

        if (Input.GetKey(KeyCode.LeftArrow))
            return Direction.Left;

        if (Input.GetKey(KeyCode.RightArrow))
            return Direction.Right;

        return null;
    }

    private bool RequestedDirectionIsOnTheSameAxis()
    {
        switch (requestedDirection.Value)
        {
            case Direction.Up:
                return currDirection == Direction.Down;

            case Direction.Down:
                return currDirection == Direction.Up;

            case Direction.Left:
                return currDirection == Direction.Right;

            case Direction.Right:
                return currDirection == Direction.Left;

            default:
                throw new System.Exception("Unexpected direction.");
        }
    }
}