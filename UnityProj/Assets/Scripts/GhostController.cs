﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class GhostController : MonoBehaviour
{
    private bool pause;
    private Direction currDirection;
    private IntVector3 intPosition;
    private int speed = 2;
    private bool isGridAligned;
    private int initialRow;
    private int initialCol;

    public Action OnTouchPlayer;

    public void OnTriggerEnter(Collider other)
    {        
        if (other.tag == "Player")
        {
            if (OnTouchPlayer != null)
                OnTouchPlayer();
        }
    }

    public void Start()
    {
        currDirection = Randomizer.GetRandomDirection();

        if (intPosition == null)
            intPosition = IntVector3.Zero;
    }

    public void Update()
    {
        if (!pause)
        {
            IntVector3 nextIntPosition = null;

            if (isGridAligned)
            {
                var availableDirections = GetAvailableDirections();

                if (availableDirections.Count == 0)
                {
                    currDirection = GetOpositeDirectionRelativeToCurrent();
                }
                else
                {
                    currDirection = Randomizer.GetOneInList<Direction>(availableDirections);
                }
            }

            nextIntPosition = GetNextPosition(currDirection);
            UpdatePosition(nextIntPosition);
        }
    }

    public void SetResetPosition(int row, int col)
    {
        initialRow = row;
        initialCol = col;

        ResetPosition();
    }

    public void ResetPosition()
    {
        var pos =
            new IntVector3(
                initialCol * GameSettings.gridSize,
                0,
                -(initialRow * GameSettings.gridSize)
            );

        UpdatePosition(pos);
    }

    public void Pause()
    {
        pause = true;
    }

    public void Unpause()
    {
        pause = false;
    }

    private bool IsFacingWall(Direction direction)
    {
        var gridPos = intPosition.ToGridPosition();

        switch (direction)
        {
            case Direction.Up:
                gridPos.Row--;
                break;

            case Direction.Down:
                gridPos.Row++;
                break;

            case Direction.Left:
                gridPos.Col--;
                break;

            case Direction.Right:
                gridPos.Col++;
                break;

            default:
                throw new Exception("Unexpected direction.");
        }

        if (gridPos.Row < 0 || 
            gridPos.Col < 0 || 
            gridPos.Row > GameSettings.rowLength - 1 ||
            gridPos.Col > GameSettings.colLength - 1)
            return true;

        var facingWall = GameSettings.level[gridPos.Row, gridPos.Col] == (int)LevelTileType.Wall;

        return facingWall;
    }

    private List<Direction> GetAvailableDirections()
    {
        var list = new List<Direction>();

        // Can go ahead?
        if (!IsFacingWall(currDirection))
            list.Add(currDirection);

        // Can go left?
        var leftDirection = GetLeftDirectionRelativeToCurrent();
        if (!IsFacingWall(leftDirection))
            list.Add(leftDirection);

        // Can go right?
        var rightDirection = GetRightDirectionRelativeToCurrent();
        if (!IsFacingWall(rightDirection))
            list.Add(rightDirection);

        return list;
    }

    private Direction GetLeftDirectionRelativeToCurrent()
    {
        switch (currDirection)
        {
            case Direction.Up:
                return Direction.Left;

            case Direction.Down:
                return Direction.Right;

            case Direction.Left:
                return Direction.Down;

            case Direction.Right:
                return Direction.Up;

            default:
                throw new Exception("Unexpected direction.");
        }
    }

    private Direction GetRightDirectionRelativeToCurrent()
    {
        switch (currDirection)
        {
            case Direction.Up:
                return Direction.Right;

            case Direction.Down:
                return Direction.Left;

            case Direction.Left:
                return Direction.Up;

            case Direction.Right:
                return Direction.Down;

            default:
                throw new Exception("Unexpected direction.");
        }
    }

    private Direction GetOpositeDirectionRelativeToCurrent()
    {
        switch (currDirection)
        {
            case Direction.Up:
                return Direction.Down;

            case Direction.Down:
                return Direction.Up;

            case Direction.Left:
                return Direction.Right;

            case Direction.Right:
                return Direction.Left;

            default:
                throw new Exception("Unexpected direction.");
        }
    }

    private void UpdatePosition(IntVector3 vec)
    {
        if (vec == null)
            throw new ArgumentNullException("vec");

        intPosition = vec;
        transform.position = intPosition.ToVector3();

        isGridAligned =
            intPosition.X % GameSettings.gridSize == 0 &&
            intPosition.Z % GameSettings.gridSize == 0;
    }

    private IntVector3 GetNextPosition(Direction direction)
    {
        var nextIntPosition = intPosition.Copy();

        switch (direction)
        {
            case Direction.Up:
                nextIntPosition.Z += speed;
                break;

            case Direction.Down:
                nextIntPosition.Z -= speed;
                break;

            case Direction.Left:
                nextIntPosition.X -= speed;
                break;

            case Direction.Right:
                nextIntPosition.X += speed;
                break;

            default:
                throw new Exception("Unexpected direction.");
        }

        return nextIntPosition;
    }
}