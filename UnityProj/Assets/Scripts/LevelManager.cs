﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject ghostPrefab;
    public GameObject pillPrefab;
    public GameObject wallSinglePrefab;
    public GameObject wallDoublePrefab;
    public GameObject wallTriplePrefab;
    public GameObject cornerDoublePrefab;
    public GameObject cornerTriplePrefab;
    public GameObject cornerQuadruplePrefab;

    public PlayerController Player { get; private set; }
    public List<PillScript> Pills { get; private set; }
    public List<GhostController> Ghosts { get; private set; }

    public void InitLevel()
    {
        Pills = new List<PillScript>();
        Ghosts = new List<GhostController>();

        var levelFolder = new GameObject("Level Objects");
        levelFolder.transform.position = Vector3.zero;

        var playerCreated = false;

        for (var row = 0; row < GameSettings.rowLength; row++)
        {
            for (var col = 0; col < GameSettings.colLength; col++)
            {
                var type = GameSettings.level[row, col];

                if (type == (int)LevelTileType.Wall)
                {
                    Quaternion rotation;
                    var wallPrefab = GetWallPrefabForCell(row, col, out rotation);
                    var wallObject = (GameObject)Instantiate(wallPrefab, GetPosition(row, col), rotation);
                    wallObject.transform.parent = levelFolder.transform;
                }
                else if (type == (int)LevelTileType.Pill)
                {
                    var pillObject = (GameObject)Instantiate(pillPrefab, GetPosition(row, col), Quaternion.identity);
                    pillObject.transform.parent = levelFolder.transform;

                    var pillScript = pillObject.GetComponent<PillScript>();
                    if (pillScript == null)
                        throw new Exception("Pill prefab does not contain PillScript component.");

                    Pills.Add(pillScript);
                }
                else if (type == (int)LevelTileType.GhostSpawn)
                {
                    var ghost = (GameObject)Instantiate(ghostPrefab, GetPosition(row, col), Quaternion.identity);

                    var ghostController = ghost.GetComponent<GhostController>();
                    if (ghostController == null)
                        throw new Exception("Ghost prefab does not contain GhostController component.");

                    ghostController.SetResetPosition(row, col);
                    Ghosts.Add(ghostController);
                }
                else if (type == (int)LevelTileType.PlayerSpawn)
                {
                    if (playerCreated)
                        throw new Exception("The level can only have one player spawn point.");

                    var playerObj = (GameObject)Instantiate(playerPrefab);
                    Player = playerObj.GetComponent<PlayerController>();

                    if (Player == null)
                        throw new Exception("PlayerController component not added to player prefab.");

                    Player.SetResetPosition(row, col);
                    playerCreated = true;
                }
            }
        }

        if (!playerCreated)
            throw new Exception("The level must have a player spawn point.");
    }

    private Vector3 GetPosition(int row, int col)
    {
        return new Vector3(col * GameSettings.gridSize, 0, -(row * GameSettings.gridSize));
    }

    private GameObject GetWallPrefabForCell(int row, int col, out Quaternion rotation)
    {
        var N = 
            row > 0 
            ? GameSettings.level[row - 1, col] == (int)LevelTileType.Wall
            : false;

        var S = 
            row < GameSettings.rowLength - 1 
            ? GameSettings.level[row + 1, col] == (int)LevelTileType.Wall 
            : false;

        var W = 
            col > 0 
            ? GameSettings.level[row, col - 1] == (int)LevelTileType.Wall 
            : false;

        var E = 
            col < GameSettings.colLength - 1 
            ? GameSettings.level[row, col + 1] == (int)LevelTileType.Wall 
            : false;

        //Debug.Log("[" + row + "," + col + "] (N:" + N + ", S:" + S + ", E:" + E + ", W:" + W + ")");

        rotation = Quaternion.identity;

        #region Wall Single

        if (!N && !S && !E && !W)
        {
            return wallSinglePrefab;
        }

        #endregion
        #region Wall Double

        if (N && !S && !E && !W)
        {
            return wallDoublePrefab;
        }

        if (!N && S && !E && !W)
        {
            rotation = Quaternion.Euler(0, 180, 0);
            return wallDoublePrefab;
        }

        if (!N && !S && E && !W)
        {
            rotation = Quaternion.Euler(0, 90, 0);
            return wallDoublePrefab;
        }

        if (!N && !S && !E && W)
        {
            rotation = Quaternion.Euler(0, 270, 0);
            return wallDoublePrefab;
        }

        #endregion
        #region Wall Triple

        if (N && S && !E && !W)
        {
            return wallTriplePrefab;
        }

        if (!N && !S && E && W)
        {
            rotation = Quaternion.Euler(0, 90, 0);
            return wallTriplePrefab;
        }

        #endregion
        #region Corner Double

        if (!N && S && !E && W)
        {
            return cornerDoublePrefab;
        }

        if (N && !S && !E && W)
        {
            rotation = Quaternion.Euler(0, 90, 0);
            return cornerDoublePrefab;
        }

        if (N && !S && E && !W)
        {
            rotation = Quaternion.Euler(0, 180, 0);
            return cornerDoublePrefab;
        }

        if (!N && S && E && !W)
        {
            rotation = Quaternion.Euler(0, 270, 0);
            return cornerDoublePrefab;
        }

        #endregion
        #region Corner Triple

        if (!N && S && E && W)
        {
            return cornerTriplePrefab;
        }

        if (N && S && !E && W)
        {
            rotation = Quaternion.Euler(0, 90, 0);
            return cornerTriplePrefab;
        }

        if (N && !S && E && W)
        {
            rotation = Quaternion.Euler(0, 180, 0);
            return cornerTriplePrefab;
        }

        if (N && S && E && !W)
        {
            rotation = Quaternion.Euler(0, 270, 0);
            return cornerTriplePrefab;
        }

        #endregion
        #region Corner Quadruple

        if (N && S && E && W)
        {
            return cornerQuadruplePrefab;
        }

        #endregion

        throw new System.Exception("Unexpected wall type.");
    }
}
