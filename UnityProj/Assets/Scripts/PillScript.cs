﻿using System;
using UnityEngine;

public class PillScript : MonoBehaviour
{
    public Action<PillScript> OnEaten;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (OnEaten != null)
                OnEaten(this);
        }
    }
}
