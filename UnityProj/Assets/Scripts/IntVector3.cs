﻿using System;
using UnityEngine;

public class IntVector3
{
    public IntVector3(int x, int y, int z)
    {
        this.X = x;
        this.Y = y;
        this.Z = z;
    }
    
    public int X { get; set; }
    public int Y { get; set; }
    public int Z { get; set; }

    public override string ToString()
    {
        return string.Format("({0}, {1}, {2})", X, Y, Z);
    }

    public IntVector3 Copy()
    {
        return new IntVector3(X, Y, Z);
    }

    public Vector3 ToVector3()
    {
        return new Vector3(X, Y, Z);
    }

    public GridPosition ToGridPosition()
    {
        var col = (int)(X / GameSettings.gridSize);
        var row = (int)(-(Z / GameSettings.gridSize));

        return new GridPosition(row, col);
    }

    public static IntVector3 Zero
    {
        get
        {
            return new IntVector3(0, 0, 0);
        }
    }
}
