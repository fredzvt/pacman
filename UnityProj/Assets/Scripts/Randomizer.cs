﻿using System;
using System.Collections.Generic;

public static class Randomizer
{
    private static Random Rnd = new Random();

    public static Direction GetRandomDirection()
    {
        return (Direction)Rnd.Next(0, 4);
    }

    public static T GetOneInList<T>(List<T> list)
    {
        if (list.Count == 1)
            return list[0];

        return list[Rnd.Next(0, list.Count)];
    }
}